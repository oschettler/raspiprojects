# Raspberry Pi Projekte

Folgende Projekte wurden bereits vorgestellt:<br>

* <a href="projects/mqtt.md">MQTT</a> (vorgestellt am 12.02.2019)
* <a href="projects/USB_Steckdosenleiste.md">USB-Steckdosenleiste</a> (vorgestellt am 19.03.2019)
* <a href="projects/KSR10_Roboterarm.md">Roboterarm</a> (vorgestellte am 16.04.2019)
* <a href="projects/Roboterarm_mit_Webcam_ansteuern.md">Roboterarm und Webcam ferngesteuert</a> und <a href="projects/feuchtigkeitssensor.md">Feuchtigkeitssensor</a> (vorgestellt am 14.05.2019)
* <a href="projects/calibre.md">EBook-Reader</a> (vorgestellt am 11.06.2019)
* <a href="projects/Motorsteuerung.md">Motorsteuerung</a> (vorgestellt am 20.08.2019)
* <a href="projects/Wlan_Anbindung.md">Wlan-Anbindung</a> (vorgestellt am 17.09.2019)
* <a href="projects/USB_Webcam.md">USB-Webcam ins Netzwerk einbinden</a> (vorgestellt am 16.10.2019)
* <a href="projects/Webradio.md">Webradio</a> (war geplant f&uuml;r 13.11.2019)
* <a href="projects/Adventskranz.md">Adventskranz mit Relais-Schaltung</a> (vorgestellt am 11.12.2019)
* <a href="#">Voice-over-IP Telefonie</a> (ist geplant f&uuml;r 21.01.2020)

Folgende Projekte sind in Vorbereitung:<br>

* <a href="https://tutorials-raspberrypi.de/automatisches-raspberry-pi-gewaechshaus-selber-bauen/">Gew&auml;chshaus</a>
* <a href="projects/USB_Festplatte.md">Externe Festplatte (USB)</a>
* <a href="https://www.golem.de/news/mitmachprojekt-temperatur-messen-und-senden-mit-dem-raspberry-pi-1604-120188.html">Zugriff auf Temperatur-PI</a>
* <a href="http://www.donkeycar.com/">Donkey-Car</a>
* <a href="https://www.einplatinencomputer.com/raspberry-pi-media-center-distributionen-im-ueberblick/">Mediacenter</a> (Musik/Video/upnp)
* <a href="https://www.kompf.de/cplus/emeocv.html">Texterkennung mit einer Webcam z.B. f&uuml;r den Stromz&auml;hler</a>
* <a href="https://tutorials-raspberrypi.de/raspberry-pi-ueberwachungskamera-livestream-einrichten/">Webcam ins Netzwerk / Internet einbinden</a>
* <a href="https://www.raspberrypi.org/blog/send-loved-ones-audio-messages/">Audio-Nachricht</a>
* <a href="http://wiki.lug-saar.de/projekte/internetradio">Webradio</a>
* <a href="https://www.makeuseof.com/tag/play-theme-tune-enter-room-raspberry-pi/">Begr&uuml;&szlig;ungssong</a>
* <a href="https://www.linuxmuster.net/wiki/anwenderwiki:infoboard:raspberry-pi">digitales Schwarzes Brett</a>
* <a href="https://pi-buch.info/bluetooth-lautsprecher-verwenden/">Bluetooth-Speaker anbinden</a>
* <a href="https://raspberry.tips/raspberrypi-tutorials/bewegungsmelder-am-raspberry-pi-auslesen">Monitor mit Bewegungsmelder steuern</a>
* <a href="https://www.tutonaut.de/anleitung-raspberry-pi-via-vnc-fernsteuern/">Remote-Desktop-Verbindung</a>
* <a href="https://hackmd.okfn.de/buchscanner_calldoku?view">Buchscanner</a>
* <a href="https://www.golem.de/news/geigerzaehler-radioaktivitaet-messen-mit-dem-raspberry-pi-1904-140654.html">Geigerz&auml;hler</a>

## Termine im Büze Ehrenfeld

Ort: <a href="http://www.buergerzentrum.info/">Bürgerzentrum Ehrenfeld</a>

Dienstag, <s>16.04.2019</s> 19:30 bis 22:00 Uhr<br>
Dienstag, <s>14.05.2019</s> 19:30 bis 22:00 Uhr <a href="https://yourpart.eu/p/5p3c98rnNy">Etherpad</a><br>
Dienstag, <s>11.06.2019</s> 19:30 bis 22:00 Uhr<br>
Dienstag, <s>20.08.2019</s> 19:30 bis 22:00 Uhr <a href="https://yourpart.eu/p/U7Tp9vjrVb">Etherpad</a><br>
Dienstag, <s>17.09.2019</s> 19:30 bis 22:00 Uhr<br>
Mittwoch, <s>16.10.2019</s> 19:30 bis 22:00 Uhr<br>
Mittwoch, <s>13.11.2019</s> 19:30 bis 22:00 Uhr<br>
Mittwoch, <s>11.12.2019</s> 19:30 bis 22:00 Uhr<br>
<br>
<b>kommende Termine:</b><br><br>
Mittwoch, <a href="https://gettogether.community/events/3574/raspberry-pi-workshop-k%C3%B6ln/">21.01.2020</a> 19:00 bis 20:00 Uhr (evtl. mit anschließendem Fachsimpeln im Büze-Cafe)<br>
