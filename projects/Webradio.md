<h2>Webradio vom 13.11.2019</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Zuerst muss man einen Webserver installieren:<br>
<code style="background:black;color:white;margin:15px">
sudo apt-get install apache2
</code>

Alternativ kann man auch lighttpd installieren:<br>
<code style="background:black;color:white;margin:15px">
sudo apt-get install lighttpd
</code>

Danach müssen die PHP-Pakete installiert werden:<br>
<code style="background:black;color:white;margin:15px">
sudo apt-get install php php-cgi
</code>

Nun wird der Benutzer pi in die Gruppe www-data hinzugefügt :<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ sudo adduser pi www-data<br>
Füge Benutzer »pi« der Gruppe »www-data« hinzu ...<br>
Benutzer pi wird zur Gruppe www-data hinzugefügt.<br>
Fertig.
</code>

und es werden noch ein paar Rechte angepasst:<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ sudo chown -R www-data:www-data /var/www<br>
pi@raspberrypi ~ $ sudo chmod -R 775 /var/www
</code>

Jetzt noch ein Neustart des Webservers:<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ sudo service apache2 restart
</code>

zum testen im Browser die IP-Adresse (z.B. localhost) oder den Hostname des RaspberryPi eingeben.

<h3> Player installieren </h3>

Für das Raspberry-Internetradio installiert man Music Player Daemon (mpd) und Music Player Command (mpc):<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ sudo apt-get install mpd mpc
</code>

Nun müssen in der Datei /etc/mpd.conf einige Einstellungen angepasst werden:<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ sudo nano /etc/mpd.conf<br>
</code>#<code style="background:black;color:white;margin:15px">log_file        "/var/log/mpd/mpd.log"                      <---- auskommentieren<br>
bind_to_address         "127.0.0.1"                       <---- ändern in 127.0.0.1<br>
</code>

Nach diesen Änderungen muss der mpd-Prozess neu gestartet werden: <br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ sudo service mpd restart
</code>

<h3> Die Playlist </h3>

Nun müssen wir die Playlist anlegen, die mit .m3u enden muss:<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ sudo nano /var/lib/mpd/playlists/internetradio.m3u
</code>

Hier ein paar Beispielstreams: <br>
<code style="background:black;color:white;margin:15px">
http://stream.laut.fm/best_of_80s<br>
http://stream.laut.fm/maximix<br>
http://stream.laut.fm/eurosmoothjazz
</code>

Eine gute Liste von Webstreams findet Ihr unter:<br>
<a href="https://wiki.ubuntuusers.de/Internetradio/Stationen">https://wiki.ubuntuusers.de/Internetradio/Stationen</a> oder<br>
<a href="https://fmstream.org/index.php">https://fmstream.org/index.php</a><br>

Die eben erstellte Playlist wird mit folgendem Kommando geladen: <br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ mpc load internetradio<br>
loading: internetradio
</code>

Beim ändern und neuladen der Webstreams muss diese vorher gelöscht werden:<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ mpc clear<br>
</code>


Zum testen kann man einen Stream von der Konsole starten:<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ mpc play 1
</code>

Und mit folgendem Befehlt kann der Stream wieder gestoppt werden:<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ mpc stop
</code>

<h3> Das Webinterface </h3>

Zuerst legen wir uns ein Verzeichnis an:<br>
<code style="background:black;color:white;margin:15px">
sudo mkdir /var/www/own/webradio
</code>

Nun laden wir das Webinterface von folgender Seite herunter:<br>
<a href="https://github.com/sn0opy/MPD-Webinterface/archive/master.zip">https://github.com/sn0opy/MPD-Webinterface/archive/master.zip</a><br>

Die Dateien müssen dann ausgepackt werden und in das angelegte webradio-Verzeichnis kopiert werden.
Anschliessend kann man in einem Browser das Webradio laufen lassen:<br>
<code style="background:black;color:white;margin:15px">
http://localhost/own/webradio
</code>

<h3> Programmgesteuertes Abspielen </h3>

Wer einen Raspberry Pi 4 mit Raspbian Buster sein eigen nennt, hat noch eine andere komfortable Möglichkeit Webradios abspielen zu können. Mit Bluetooth und der Audio-Konfiguration kann man dann das Webradio sogar auf einem externen Bluetooth-Lautsprecher ausgeben. Dazu installiert man sich folgends Programm:<br>

<code style="background:black;color:white;margin:15px">
sudo apt-get install smplayer
</code>

Im Menü von SMPlayer gibt es einen Eintrag für Radiolisten, dort kann man seine Webradio-Stream eintragen und aufrufen.

