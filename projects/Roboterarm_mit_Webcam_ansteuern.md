<h2>KSR10 Roboterarm mit einer Webcam fernsteuern vom 14.05.2019</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Basierend auf den letzten <a href="KSR10_Roboterarm.md">Workshop</a> soll an den Roboterarm eine Webcam installiert werden. Beides soll mit einem <a href="https://gitlab.com/horald/raspiprojects/blob/master/python/ksr10_Webinterface.py">Webinterface</a> ferngesteuert werden. F&uuml;r das Webinterface muss die Bibliothek <a href="https://wiki.ubuntuusers.de/Bottle/">bottle</a> installiert werden.
F&uuml;r die Webcam muss das Programm <a href="https://motion-project.github.io/">motion</a> installiert sein. Mit einem <a href="https://www.w3schools.com/tags/tag_iframe.asp">iframe</a> wird die Webcam dann in das Webinterface integriert. 
</br></br>
<h3>Installation von bottle</h3>
<code style="background:black;color:white;padding:5px">
 pip install bottle 
</code>
<h3>Installation von motion</h3>
<code style="background:black;color:white;padding:5px">
 sudo apt-get install motion
</code>