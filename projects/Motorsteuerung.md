<h2>Motorsteuerung vom 20.08.2019</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Hier wird beschrieben wie man mit einem Roboter-Fahrwerk und dem Raspberry Pi die Motoren ansteuern und somit das Fahrzeug fahren lassen kann.

Folgende Komponenten werden benötigt:
<ul>
<li> <a href="https://www.robotshop.com/de/de/2wd-anfanger-roboter-fahrwerk.html">Bausatz Roboter-Fahrwerk</a></li>
<li> <a href="https://www.roboter-bausatz.de/143/l298n-motortreiber-mit-doppelter-h-bruecke?number=RBS10092">Motortreiber</a></li>
<li> 4xAA Batterien (1.5V)</li>
<li> Powerbank</li>
<li> <a href="https://www.robotshop.com/de/de/125-35v-einstellbarer-abwartsspannungsregler-lm2596s.html">Spannungsregler</a></li>
<li> Jumper Kabel</li>
<li> Lötutensilien (Lötkolben, Lötzinn, Unterlage, etc.)</li>
</ul>

So sieht das vorläufig fertige Fahrwerk aus:<br>
<img src="../images/Raspberry-Pi-Roboter-Bausatz.jpg" alt="Fahrwerk" width="300" height="200">


Zum ansteuern der Motoren kann folgends Python-Script verwendet werden:
<a href="../python/motor.py">motor.py</a><br>