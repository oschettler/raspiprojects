<nowiki>
from Tkinter import *
from datetime import timedelta, date
import sys 

# Ein Fenster erstellen
fenster = Tk()
# Den Fenstertitle erstellen
fenster.title("Adventskranz")

now = date.today()
tag=now.strftime("%d")
monat=now.strftime("%m")

wndate=date(int(jahr), 12, 25)
wotag=int(wndate.weekday())+1
advdate1 = wndate-timedelta(days=wotag)-timedelta(days=21)
advwotag1=advdate1.strftime("%d")
advdate2 = wndate-timedelta(days=wotag)-timedelta(days=14)
advwotag2=advdate2.strftime("%d")
advdate3 = wndate-timedelta(days=wotag)-timedelta(days=7)
advwotag3=advdate3.strftime("%d")
advdate4 = wndate-timedelta(days=wotag)
advwotag4=advdate4.strftime("%d")

anz_kerzen=0
if (tag>=advwotag1 and monat=="12") or (advwotag1>=str(25) and monat=="11"):
  anz_kerzen=1
if tag>=advwotag2 and monat=="12":
  anz_kerzen=2
if tag>=advwotag3 and monat=="12":
  anz_kerzen=3
if tag>=advwotag4 and monat=="12":
  anz_kerzen=4  


if anz_kerzen>0:
  print("relais1.an")
else:  
  print("relais1.aus")
if anz_kerzen>1:
  print("relais2.an")
else:  
  print("relais2.aus")
if anz_kerzen>2:
  print("relais3.an")
else:  
  print("relais3.aus")
if anz_kerzen>3:
  print("relais4.an")
else:  
  print("relais4.aus")

fenster.mainloop()


