<h2>Adventskranz</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

<h3> Anschaffung und Einrichtung </h3>

Für diesen Adventskranz werden folgende Komponenten benötigt:

* Raspberry Pi
* Netzstecker
* Lautsprecherkabel (4x0,5m=2m)
* LED-Kerzen
* Relaiskarte GPIO
* Batterien für die LED-Kerzen
* Jumperkabel
* 4 kleine Stücke Pappe

Die Lautsprecherkabel werden an beiden Seiten abisoliert. In die Batterieabdeckung der LED-Kerzen werden jeweils ein Loch in der Größe der Lautsprecherkabel gebohrt. Bei mir ca. 4,5mm. Zwischen die zwei blanken Drahtenden wird ein Stück Pappe zur Isolierung geklemmt. Dann werden die Drahtenden in das Batteriefach der LED-Kerze zwischen Anschlußpol und Batterie geklemmt. Nun wird das Kabel durch das Loch der Batterieabdeckung gesteckt und das Batteriefach geschlossen. 
Die anderen Enden des Kabels werden dann jeweils in ein Relais verschraubt:<br><br>
<img src="../images/Adventskranz.jpg" alt="Adventskranz"><br><br>
Die Relaiskarte wird mit dem Jumperkabel und dem Raspberry Pi verbunden. Wenn wir jetzt den Netzstecker mit dem Raspberry Pi verbinden und in eine Steckdose stecken, wird das unten aufgeführte Adventsprogramm gestartet und eine oder mehrere Kerzen des Adventskranz leuchten je nach Datum. Hier ein Beispiel für die Verkabelung:<br><br>
<img src="../images/4relais-raspberry.png"><br>


<h3> Adventsprogramm </h3>

Für den Adventskranz schreiben wir folgendes Adventsprogramm:<br>
<code style="background:gray;color:white;margin:15px">
<a href="#">relaisadventskranz.py</a> (wird noch ergänzt)
</code>


<h3> Autostart </h3>

Damit der Raspberry Pi nicht permanent an sein muss, werden wir ihn so einrichten, dass nach dem Booten automatisch das Adventsprogramm starten. Dann kann man den Netzstecker z.B. in eine Schaltuhr stecken, die nur Sonntags eingeschaltet wird.

Dazu editieren wir folgende Datei:<br>
<code style="background:black;color:white;margin:15px">
pi@raspberrypi ~ $ sudo nano /etc/rc.local
</code>

Hier ein Beispiele was in der Datei stehen muss. Am Ende der Datei muss immer exit 0 stehen, eure Einträge müssen daher davor gesetzt werden. Zuerst muss mal das aktuelle Datum aus dem Netzwerk geholt werden:<br>
<code style="background:darkblue;color:yellow;margin:15px">
date... (wird noch ergänzt)
</code>

Dann muss der Aufruf auf das Adventsprogramm erfolgen:<br>
<code style="background:darkblue;color:yellow;margin:15px">
python Adventskranz.py
</code>

Ich wünsche einen schönen Advent.


