<h2>USB-Webcam ins Netzwerk einbinden vom 16.10.2019</h2>

<a href="../liesmich.md">Raspberry Pi Projekte</a><br><br>

Um den Anschluß der Webcam am USB-Port zu überprüfen, können wir folgenden Befehl eingeben:

<code style="background:black;color:white;margin:15px">
lsusb
</code>

Wenn die Webcam erkannt wurde, kann man mit <a href="https://wiki.ubuntuusers.de/Cheese/">Cheese</a> überprüfen, ob die Webcam auch funktioniert. Dieses kleine Programm installieren wir mit folgendem Befehl:

<code style="background:black;color:white;padding:15px">
sudo apt-get install cheese
</code>

Sofern ein Bild angezeigt wird, kann nun das Livestream-Tool <a href="https://motion-project.github.io/">Motion</a> installiert werden:

<code style="background:black;color:white;margin:15px">
sudo apt-get install motion
</code>

In der Konfigurationsdatei von Motion müssen vorher noch einige Einstellungen vorgenommen werden:

<code style="background:black;color:white;margin:15px">
sudo nano /etc/motion/motion.conf
</code>

### Start im Daemon-(Hintergrund-)Modus und Freigabe des Terminals (Standard: aus)
<b>daemon on</b>
...
### Stream-Verbindungen nur auf den lokalen Host beschränken (Standard: ein)
<b>stream_localhost off</b>
...
### Zielbasisverzeichnis für Bilder und Filme
### Es wird empfohlen, den absoluten Pfad zu verwenden. (Standard: aktuelles Arbeitsverzeichnis)
<b>target_dir /home/pi/Monitor</b>

### Bildbreite (Pixel). Gültiger Bereich: Kameraabhängig, Standard: 352 
<b>width 640</b> 

### Bildhöhe (Pixel). Gültiger Bereich: Kameraabhängig, Standard: 288 
<b>height 480</b> 

### Maximale Anzahl der Bilder, die pro Sekunde aufgenommen werden sollen. 
### Gültiger Bereich: 2-100. Standard: 100 (fast unbegrenzt). 
<b>framerate 10</b> 

Jetzt müssen wir nur noch den Daemon aktivieren, damit wir den Service gleich laufen lassen können:

<code style="background:black;color:white;margin:15px">
sudo nano /etc/default/motion
</code>

<b>start_motion_daemon=yes</b>

Anschließend können wir den Service bereits starten:

<code style="background:black;color:white;margin:15px">
sudo service motion start
</code>

## *** Testen des Livestreams ***

Um den Livestream zu testen, kann man die lokale IP-Adresse der Rasperry Pi's verwenden, mit der Standard-Port-Adresse 8081 von Motion.
Die IP-Adresse kann man mit folgendem Befehl abfragen:

<code style="background:black;color:white;margin:15px">
ifconfig
</code>

Der Aufruf im Browser lautet dann z.B.:

http://192.168.1.51:8081/ 

## *** Ins Internet stellen ***

Um die Webcam ins Internet zu stellen, gibt es mehrere Möglichkeiten. Wer eine eigne Website hat, kann z.B. mit ftp, mit Webdav oder recht einfach mit sshfs eine Anbindung herstellen.
Ansonsten bietet sich noch die Möglichkeit mit <a href=https://www.noip.com/">NO-IP</a> zu arbeiten.

Für weitere Informationen siehe folgende Links: <br>

* <a href="https://tutorials-raspberrypi.de/raspberry-pi-ueberwachungskamera-livestream-einrichten/">Webcam Tutorial</a>
