from Tkinter import *
from datetime import timedelta, date
import sys 

# Ein Fenster erstellen
fenster = Tk()
# Den Fenstertitle erstellen
fenster.title("Adventskranz")

canvas_width = 190
canvas_height =160

now = date.today()
tag=now.strftime("%d")
monat=now.strftime("%m")
jahr=now.strftime("%Y")
datum=now.strftime("%d.%m.%Y")

wndate=date(int(jahr), 12, 25)
wotag=int(wndate.weekday())+1
actwotag=int(now.weekday())
advdate1 = wndate-timedelta(days=wotag)-timedelta(days=21)
advwotag1=advdate1.strftime("%d")
advdate2 = wndate-timedelta(days=wotag)-timedelta(days=14)
advwotag2=advdate2.strftime("%d")
advdate3 = wndate-timedelta(days=wotag)-timedelta(days=7)
advwotag3=advdate3.strftime("%d")
advdate4 = wndate-timedelta(days=wotag)
advwotag4=advdate4.strftime("%d")

anz_kerzen=0
if (tag>=advwotag1 and monat=="12") or (advwotag1>=str(25) and monat=="11"):
  anz_kerzen=1
if tag>=advwotag2 and monat=="12":
  anz_kerzen=2
if tag>=advwotag3 and monat=="12":
  anz_kerzen=3
if tag>=advwotag4 and monat=="12":
  anz_kerzen=4  

w = Canvas(fenster, 
           width=canvas_width, 
           height=canvas_height)
w.pack()


if anz_kerzen>0:
  w.create_oval(40,30-offset1,60,60-offset1, fill="yellow")
else:  
  w.create_rectangle(48, 50-offset1, 52, 60-offset1, fill="black")
if anz_kerzen>1:
  w.create_oval(70,30-offset2,90,60-offset2, fill="yellow")
else:  
  w.create_rectangle(78, 50-offset2, 82, 60-offset2, fill="black")
if anz_kerzen>2:
  w.create_oval(100,30-offset3,120,60-offset3, fill="yellow")
else:  
  w.create_rectangle(108, 50-offset3, 112, 60-offset3, fill="black")
if anz_kerzen>3:
  w.create_oval(130,30-offset4,150,60-offset4, fill="yellow")
else:  
  w.create_rectangle(138, 50-offset4, 142, 60-offset4, fill="black")

w.create_rectangle(40, 65-offset1, 60, 130-offset1, fill="red")
w.create_rectangle(70, 65-offset2, 90, 130-offset2, fill="red")
w.create_rectangle(100, 65-offset3, 120, 130-offset3, fill="red")
w.create_rectangle(130, 65-offset4, 150, 130-offset4, fill="red")
heute_label = Label(fenster, text="Heute ist "+wotagstr+" der "+datum)
heute_label.pack()
advent_label = Label(fenster, text="Heute ist der "+str(anz_kerzen)+". Advent oder danach.")
advent_label.pack()

# In der Ereignisschleife auf Eingabe des Benutzers warten.
fenster.mainloop()


