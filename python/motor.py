import sys
import time
import RPi.GPIO as GPIO

mode=GPIO.getmode()

GPIO.cleanup()

LeftForward=38
LeftBackward=36
LeftEnable=32
RightForward=35
RightBackward=37
RightEnable=31
sleeptime=1

GPIO.setmode(GPIO.BOARD)
GPIO.setup(LeftForward, GPIO.OUT)
GPIO.setup(LeftBackward, GPIO.OUT)
GPIO.setup(LeftEnable, GPIO.OUT)
GPIO.setup(RightForward, GPIO.OUT)
GPIO.setup(RightBackward, GPIO.OUT)
GPIO.setup(RightEnable, GPIO.OUT)

def forward(x):
  GPIO.output(LeftBackward, GPIO.LOW)
  GPIO.output(RightBackward, GPIO.LOW)
  GPIO.output(LeftForward, GPIO.HIGH)
  GPIO.output(RightForward, GPIO.HIGH)
  GPIO.output(LeftEnable, GPIO.HIGH)
  GPIO.output(RightEnable, GPIO.HIGH)
  print("Moving Forward")
  time.sleep(x)
  GPIO.output(LeftForward, GPIO.LOW)
  GPIO.output(RightForward, GPIO.LOW)

#def reverse(x):
#  GPIO.output(Backward, GPIO.HIGH)
#  GPIO.output(Backward1, GPIO.HIGH)
#  print("Moving Backward")
#  timesleep(x)
#  GPIO.output(Backward, GPIO.LOW)
#  GPIO.output(Backward1, GPIO.LOW)

#while (1):

forward(2)

#reverse(5)

GPIO.cleanup()
