import atexit
import time
import sqlite3
import ksr10
from Tkinter import *
from tkMessageBox import *

class App:

  def goodbye(name):
    #print 'Goodbye, %s, it was %s to meet you.' % (name, adjective)
    showinfo('Hinweis', 'Ich sage '+name.get())

	
  def __init__(self, master):
  	 #atexit.register(goodbye, 'tschuess')
    frame = Frame(master)
    frame.pack()
    self.conn = sqlite3.connect('data/roboterarm.db')
    self.button = Button(frame, 
                         text="Tschuess", fg="red",
                         command=frame.quit)
    self.button.grid(row=0, column=0, pady = 20)
    #self.button.pack(side=LEFT)

    self.slogan = Button(frame,
                         text="Licht",
                         command=self.steuer_licht)
    self.slogan.grid(row=1, column=0, pady = 20)

    self.textlabel = Label(frame, text="Sekunden : ")
    self.textlabel.grid(row=0, column=1, pady = 20)

    self.defval = StringVar(frame, value='1')
    self.eingabefeld = Entry(frame, textvariable=self.defval, bd=5, width=40)
    self.eingabefeld.grid(row=1, column=1, pady = 20)

    self.slogan = Button(frame,
                         text="Rumpf nach links",
                         command=self.steuer_base_left)
    self.slogan.grid(row=2, column=0, pady = 20)

    self.slogan = Button(frame,
                         text="Rumpf nach rechts",
                         command=self.steuer_base_right)
    self.slogan.grid(row=2, column=1, pady = 20)

    self.slogan = Button(frame,
                         text="Oberarm nach links",
                         command=self.steuer_grip_left)
    self.slogan.grid(row=3, column=0, pady = 20)

    self.slogan = Button(frame,
                         text="Oberarm nach rechts",
                         command=self.steuer_grip_right)
    self.slogan.grid(row=3, column=1, pady = 20)

    self.slogan = Button(frame,
                         text="Unterarm nach links",
                         command=self.steuer_wrist_right)
    self.slogan.grid(row=4, column=0, pady = 20)

    self.slogan = Button(frame,
                         text="Unterarm nach rechts",
                         command=self.steuer_wrist_left)
    self.slogan.grid(row=4, column=1, pady = 20)

    self.slogan = Button(frame,
                         text="Stecken",
                         command=self.steuer_strecken)
    self.slogan.grid(row=5, column=0, pady = 20)

    self.slogan = Button(frame,
                         text="Beugen",
                         command=self.steuer_beugen)
    self.slogan.grid(row=5, column=1, pady = 20)

    self.slogan = Button(frame,
                         text="Handgelenk nach oben",
                         command=self.steuer_elbow_down)
    self.slogan.grid(row=6, column=0, pady = 20)

    self.slogan = Button(frame,
                         text="Handgelenk nach unten",
                         command=self.steuer_elbow_up)
    self.slogan.grid(row=6, column=1, pady = 20)

    self.slogan = Button(frame,
                         text="Greifer auf",
                         command=self.steuer_grip_open)
    self.slogan.grid(row=7, column=0, pady = 20)

    self.slogan = Button(frame,
                         text="Greifer zu",
                         command=self.steuer_grip_close)
    self.slogan.grid(row=7, column=1, pady = 20)
    self.CheckVar1 = IntVar()
    self.C1 = Checkbutton(frame, text = "Aufzeichnen", variable = self.CheckVar1, \
                 onvalue = 1, offvalue = 0, height=5, \
                 width = 20)
    self.C1.grid(row=8, column=0, pady = 20)   

    sqlval = StringVar(frame, value='Test')
    self.sqlfeld = Entry(frame, textvariable=sqlval, bd=5, width=40)
    self.sqlfeld.grid(row=1, column=2, pady = 20)
    self.slogan = Button(frame,
                         text="Auslesen",
                         command=self.befehle_auslesen)
    self.slogan.grid(row=8, column=1, pady = 20)       
    self.slogan = Button(frame,
                         text="Invertiertes Auslesen",
                         command=self.befehle_invertiert_auslesen)
    self.slogan.grid(row=8, column=2, pady = 20)       
    self.slogan = Button(frame,
                         text="Loeschen",
                         command=self.eintrag_loeschen)
    self.slogan.grid(row=7, column=2, pady = 20)  
    
    self.update_button = Button(frame, text='Update option menu', command=self.update_option_menu)
    self.update_button.grid(row=6, column=2, pady = 20)
    
         
    self.var = StringVar(frame)
    self.var.set("Auswahl")
    self.choices = {} 
    c = self.conn.cursor()
    for row in c.execute('SELECT distinct fldbez FROM tblroboterarm'):
    	self.choices.setdefault(row[0])
    self.popupMenu = OptionMenu(frame, self.var, *self.choices) 
    self.popupMenu.grid(row=2, column=2, pady = 20)       

  def update_option_menu(self):
		print("update_option_menu")  	
		self.choices = {} 
		c = self.conn.cursor()
		for row in c.execute('SELECT distinct fldbez FROM tblroboterarm'):
			self.choices.setdefault(row[0])
		menu = self.popupMenu["menu"]
		menu.delete(0, "end")
		for string in self.choices:
			menu.add_command(label=string, 
									command=lambda value=string: self.var.set(value))

  def eintrag_loeschen(self):
		print("Eintrag loeschen:"+self.var.get())
		c = self.conn.cursor()
		t = (self.var.get(),)
		c.execute("DELETE FROM tblroboterarm WHERE fldbez=?", t)
		self.conn.commit()
      
  def befehle_auslesen(self):
		print("auslesen")
		c = self.conn.cursor()
		t = (self.var.get(),)
		for row in c.execute('SELECT * FROM tblroboterarm WHERE fldbez=? ORDER BY fldindex', t):
			print(row)
			self.defval.set(row[3])
			if row[2]=='base left':
				self.steuer_base_left()
			elif row[2]=='base right':
				self.steuer_base_right()
			elif row[2]=='grip up':
				self.steuer_grip_left()
			elif row[2]=='grip down':
				self.steuer_grip_right()
			elif row[2]=='wrist left':
				self.steuer_wrist_left()
			elif row[2]=='wrist right':
				self.steuer_wrist_right()
			elif row[2]=='elbow up':
				self.steuer_elbow_up()
			elif row[2]=='elbow down':
				self.steuer_elbow_down()

  def befehle_invertiert_auslesen(self):
		print("invertiert auslesen")
		c = self.conn.cursor()
		t = (self.var.get(),)
		for row in c.execute('SELECT * FROM tblroboterarm WHERE fldbez=? ORDER BY fldindex DESC', t):
			print(row)
			self.defval.set(row[3])
			if row[2]=='base left':
				self.steuer_base_right()
			elif row[2]=='base right':
				self.steuer_base_left()
			elif row[2]=='grip up':
				self.steuer_grip_right()
			elif row[2]=='grip down':
				self.steuer_grip_left()
			elif row[2]=='wrist left':
				self.steuer_wrist_right()
			elif row[2]=='wrist right':
				self.steuer_wrist_left()
			elif row[2]=='elbow up':
				self.steuer_elbow_down()
			elif row[2]=='elbow down':
				self.steuer_elbow_up()


  def steuer_base_left(self):
    #showinfo('Hinweis', 'base left Sek:'+self.eingabefeld.get())
    ksr.move("base","left")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen base left")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','base left',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_base_right(self):
    #showinfo('Hinweis', 'base right Sek:'+self.eingabefeld.get())
    ksr.move("base","right")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen base right")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','base right',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_grip_left(self):
    #showinfo('Hinweis', 'grip up Sek:'+self.eingabefeld.get())
    ksr.move("grip","up")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen grip up")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','grip up',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_grip_right(self):
    #showinfo('Hinweis', 'grip down Sek:'+self.eingabefeld.get())
    ksr.move("grip","down")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen grip down")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','grip down',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_wrist_left(self):
    #showinfo('Hinweis', 'wrist left Sek:'+self.eingabefeld.get())
    ksr.move("wrist","left")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen wrist left")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','wrist left',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_wrist_right(self):
    #showinfo('Hinweis', 'wrist right Sek:'+self.eingabefeld.get())
    ksr.move("wrist","right")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen wrist right")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','wrist right',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_elbow_up(self):
    #showinfo('Hinweis', 'elbow up Sek:'+self.eingabefeld.get())
    ksr.move("elbow","up")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen elbow up")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','elbow up',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_elbow_down(self):
    #showinfo('Hinweis', 'elbow donw Sek:'+self.eingabefeld.get())
    ksr.move("elbow","down")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen elbow down")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','elbow down',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_grip_open(self):
    #showinfo('Hinweis', 'grip open Sek:'+self.eingabefeld.get())
    ksr.move("shoulder","open")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen shoulder open")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','shoulder open',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_grip_close(self):
    #showinfo('Hinweis', 'grip close Sek:'+self.eingabefeld.get())
    ksr.move("shoulder","close")
    time.sleep(float(self.eingabefeld.get()))
    ksr.stop() 
    if self.CheckVar1.get() == 1:
      print("aufzeichnen shoulder close")
      c = self.conn.cursor()
      c.execute("INSERT INTO tblroboterarm (fldbez,fldbefehl,fldsek) VALUES ('"+self.sqlfeld.get()+"','shoulder close',"+self.eingabefeld.get()+")")
      self.conn.commit()

  def steuer_strecken(self):
    #showinfo('Hinweis', 'strecken Sek:'+self.eingabefeld.get())
    bis = 5 * int(self.eingabefeld.get())
    for i in range(1, bis+1):

      ksr.move("wrist","down")
      time.sleep(0.2)
      ksr.stop() 

      ksr.move("grip","up")
      time.sleep(0.2)
      ksr.stop() 

  def steuer_beugen(self):
    #showinfo('Hinweis', 'beugen Sek:'+self.eingabefeld.get())
    bis = 5 * int(self.eingabefeld.get())
    for i in range(1, bis+1):

      ksr.move("wrist","up")
      time.sleep(0.2)
      ksr.stop() 

      ksr.move("grip","down")
      time.sleep(0.2)
      ksr.stop() 

  def steuer_licht(self):
      ksr.lights()

ksr = ksr10.ksr10_class()
root = Tk()
root.title("KSR10 Steuerpult")
app = App(root)
root.mainloop()